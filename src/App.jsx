import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import SearchBar from './components/SearchBar';
import ArtistPage from './components/ArtistPage';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import { getArtistAlbums, searchArtist } from './services/spotifyService';
import { sections } from './config.json';

const useStyles = makeStyles((theme) => ({
  app: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#443c49',
    minHeight: '100%',
    padding: '2%',
  },
  section: {
    flexGrow: 1,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '2%',
    borderRadius: 18,
    backgroundColor: '#00000044',
    transition: [['all', '1000ms']],
  },
  collapsed: {
    flexGrow: 0.00001,
  },
  expanding: {
    marginTop: 20,
    maxHeight: 0,
    overflow: 'hidden',
    justifyContent: 'flex-start',
    opacity: 1,
    transition: [['opacity', '500ms']],
    animation: '$grow 2000ms',
    animationFillMode: 'forwards',
  },
  transparent: {
    opacity: 0,
  },
  '@keyframes grow': {
    '0%': {
      maxHeight: 0,
    },
    '99%': {
      maxHeight: '200vh',
    },
    '100%': {
      maxHeight: 'none',
    },
  },
  container: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const SearchSection = (classes, currentSection, onSearch, searchMessage) => {
  return (
    <Paper
      elevation={2}
      className={`${classes.section} ${
        currentSection === sections.search ? '' : classes.collapsed
      }`}
    >
      <div className={classes.container}>
        <SearchBar
          placeHolder="Search for an artist"
          onSearch={onSearch}
          allowEmpty={false}
          message={searchMessage}
        />
      </div>
    </Paper>
  );
};

const ArtistSection = (
  classes,
  currentArtist,
  currentSection,
  artistTransparent,
  onUpdate,
  canUpdate
) => {
  if (currentSection === sections.artist) {
    return (
      <Paper
        elevation={2}
        className={`${classes.section} ${classes.expanding}`}
      >
        <ArtistPage
          artist={currentArtist}
          onUpdate={onUpdate}
          transparent={artistTransparent}
          canUpdate={canUpdate}
        />
      </Paper>
    );
  } else return '';
};

const App = () => {
  const classes = useStyles();
  const [currentSection, setCurrentSection] = React.useState(sections.search);
  const [currentArtist, setCurrentArtist] = React.useState(null);
  const [searchMessage, setSearchMessage] = React.useState('');
  const [artistTransparent, setArtistTransparent] = React.useState(false);
  let artistCanUpdate = false;

  const onSearch = async (query) => {
    try {
      if (currentSection === sections.search) {
        const artist = await searchArtist(query);
        const albums = await getArtistAlbums(artist);
        artist.albums = albums;

        setCurrentArtist(artist);
        setCurrentSection(sections.artist);
      } else {
        setArtistTransparent(true);
        const artist = await searchArtist(query);
        const albums = await getArtistAlbums(artist);
        artist.albums = albums;
        setTimeout(() => {
          setCurrentArtist(artist);
          artistCanUpdate = true;
          setCurrentSection(sections.artist);
        }, 500);
      }

      setSearchMessage('');
    } catch (error) {
      setSearchMessage(error);
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <div className={classes.app}>
        {SearchSection(classes, currentSection, onSearch, searchMessage)}

        {ArtistSection(
          classes,
          currentArtist,
          currentSection,
          artistTransparent,
          () => setArtistTransparent(false),
          artistCanUpdate
        )}
      </div>
    </React.Fragment>
  );
};

export default App;
