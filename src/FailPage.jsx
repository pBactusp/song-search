import React from 'react';
import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  page: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#443c49',
    minHeight: '100%',
    padding: '2%',
  },
  section: {
    flexGrow: 1,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '2%',
    borderRadius: 18,
    backgroundColor: '#00000044',
    transition: [['all', '1000ms']],
  },
  container: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  message: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#db7867',
    fontSize: '200%',
  },
}));

const FailPage = ({ message }) => {
  const classes = useStyles();

  return (
    <div className={classes.page}>
      <Paper className={classes.section} elevation={2}>
        <label className={classes.message}>{message}</label>
      </Paper>
    </div>
  );
};

FailPage.defaultProps = {
  message: 'There was a problem!',
};

export default FailPage;
