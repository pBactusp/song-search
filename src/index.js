import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import FailPage from './FailPage';
import { init } from './services/spotifyService';

async function main() {
  try {
    await init();
    return <App />;
  } catch (error) {
    return <FailPage message={error.message} />;
  }
}

main().then((page) => ReactDOM.render(page, document.getElementById('root')));
