import Spotify from 'spotify-web-api-js';
import { urls } from '../config.json';

let _initialized = false;

const spotifyApi = new Spotify();
const client = {
  id: process.env.REACT_APP_CLIENT_ID,
  secret: process.env.REACT_APP_CLIENT_SECRET,
};

function setExtentions(items) {
  for (const index in items) {
    const item = items[index];
    item.fullName = item.name;

    let extMatch = item.name.match(/\(.*\)$/g);
    if (!extMatch || extMatch.length === 0) {
      extMatch = item.name.match(/(( *-+ *)|(: *))([lL]ive).*$/g);
      if (!extMatch || extMatch.length === 0)
        extMatch = item.name.match(/ +([lL]ive).*$/g);
    }

    if (
      extMatch &&
      extMatch.length > 0 &&
      extMatch[0].length < item.name.length
    ) {
      item.extention = extMatch[0];
      item.name = item.name.slice(0, item.name.length - item.extention.length);
    }
  }
}
function containsAlbum(albums, album) {
  for (const index in albums) {
    if (albums[index].name.toLowerCase() === album.name.toLowerCase())
      return true;
  }

  return false;
}

function filterDuplicateAlbums(albums) {
  const filtered = [];

  for (const index in albums) {
    if (!containsAlbum(filtered, albums[index])) {
      filtered.push(albums[index]);
    }
  }

  return filtered;
}

async function requestToken(client) {
  const result = await fetch(urls.spotifyAuth, {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic ' + btoa(client.id + ':' + client.secret),
    },
    body: 'grant_type=client_credentials',
  });

  const data = await result.json();

  if (result.status === 200) return data;

  throw data;
}

async function _init(spotifyApi, client) {
  try {
    const token = await requestToken(client);
    spotifyApi.setAccessToken(token.access_token);
  } catch {
    throw new Error(
      'Cannot access the server at the moment. Please try again later.'
    );
  }
}

export async function init() {
  try {
    await _init(spotifyApi, client);
    _initialized = true;
  } catch (error) {
    throw error;
  }
}

export async function searchArtist(query) {
  if (!_initialized) throw new Error('Not connected to server');

  const { artists } = await spotifyApi.searchArtists(query.trim(), {
    limit: 1,
  });

  if (artists.items.length > 0) {
    const artist = artists.items[0];
    return {
      id: artist.id,
      name: artist.name,
      images: [...artist.images],
    };
  }

  throw `Could not find an artist by the name '${query}'`;
}

export async function getArtistAlbums(artist) {
  if (!_initialized) throw new Error('Not connected to server');

  const { items: albums } = await spotifyApi.getArtistAlbums(artist.id, {
    include_groups: 'album,single',
  });

  if (albums) {
    setExtentions(albums);
    return filterDuplicateAlbums(albums);
  }
  throw `Could not find albums by the artist '${artist.name}'`;
}

export async function getAlbumTracks(album) {
  if (!_initialized) throw new Error('Not connected to server');

  const { items: tracks } = await spotifyApi.getAlbumTracks(album.id, {});

  if (tracks) {
    setExtentions(tracks);
    return tracks;
  }

  throw `Could not find songs in the album '${album.name}'`;
}
