import { urls } from '../config.json';

export async function requestLyrics(artist, title) {
  try {
    const url = `${urls.lyricsApi}/${artist}/${title}`;
    const result = await fetch(url);
    const data = await result.json();

    return data.lyrics;
  } catch (error) {
    throw `Could not find lyrics for the song '${title}' by '${artist}'`;
  }
}
