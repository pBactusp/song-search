import React, { useRef } from 'react';
import { Paper } from '@material-ui/core';
import { Box } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import Album from './Album';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    transition: [['all', '500ms']],
  },
  avatar: {
    width: '40%',
    height: 'auto',

    border: '10px solid #bbbbbb44',
  },
  name: {
    textAlign: 'center',
    marginTop: '3%',
    fontWeight: 'bold',
    color: '#48B37E',
    fontSize: '250%',
  },
  artistContainer: {
    flexGrow: '1',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  albumsContainer: {
    flexGrow: '1',
    marginTop: '3%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    padding: '2%',
    borderRadius: 5,
    backgroundColor: '#bbbbbb44',
  },
  transparent: {
    opacity: 0,
  },
}));

const ArtistPage = ({ artist, onUpdate, transparent }) => {
  const classes = useStyles();
  const isInitialMount = useRef(true);

  const handleImageUpdate = () => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      onUpdate && onUpdate();
    }
  };

  if (artist) {
    const imageUrl =
      artist && artist.images.length > 0 ? artist.images[1].url : '';
    const albums = artist.albums;

    const displayAlbums = () => {
      const components = [];
      for (const album in albums) {
        components.push(
          <Album
            key={albums[album].id}
            album={albums[album]}
            artistName={artist.name}
          />
        );
      }

      return components;
    };

    return (
      <Box className={`${classes.root} ${transparent && classes.transparent}`}>
        <div className={classes.artistContainer}>
          <Avatar
            className={classes.avatar}
            src={imageUrl}
            alt={artist.name}
            imgProps={{ onLoad: handleImageUpdate }}
          />
          <label className={classes.name}>{artist.name}</label>
        </div>
        <Paper className={classes.albumsContainer}>{displayAlbums()}</Paper>
      </Box>
    );
  }

  return '';
};

ArtistPage.defaultProps = {
  onUpdate: null,
  canUpdate: false,
};

export default ArtistPage;
