import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: 30,
    display: 'flex',
    alignItems: 'center',
    transition: [['background-color', '100ms']],
    '&:hover': {
      backgroundColor: '#ffffff44',
    },
  },
  text: {
    marginLeft: 10,
  },
}));

const handleClick = (text, onClick) => {
  if (onClick) onClick(text);
};

const StyledText = ({ text, onClick }) => {
  const classes = useStyles();

  return (
    <Box
      className={classes.root}
      onMouseDown={() => handleClick(text, onClick)}
    >
      <label className={classes.text}>{text}</label>
    </Box>
  );
};

StyledText.defaultProps = {
  text: '',
  onClick: null,
};

export default StyledText;
