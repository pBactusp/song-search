import React from 'react';
import { Box } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
  },
  divider: {
    borderBottom: '3px solid grey',
  },
  smalldivider: {
    borderBottom: '2px solid grey',
    margin: '0 5px',
  },
  topBar: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  button: {
    color: 'grey',
  },
  trackName: {
    fontSize: '130%',
  },
  artistName: {
    marginLeft: 5,
  },
  titleContainer: {
    fontWeight: 'bold',
  },
  lyrics: {
    whiteSpace: 'pre-wrap',
    margin: '8px 8px',
  },
  empty: {
    fontWeight: 'bold',
    color: '#db7867',
  },
}));

const displayLyrics = (track, lyricsClass, emptyClass) => {
  if (track.lyrics !== '')
    return <label className={lyricsClass}>{track.lyrics}</label>;
  else
    return (
      <label className={`${lyricsClass} ${emptyClass}`}>
        Unable to find lyrics for ths song.
      </label>
    );
};

const LyricsDisplay = ({ artistName, track, onClick }) => {
  const classes = useStyles();

  if (track) {
    return (
      <Box className={classes.root}>
        <div className={classes.divider}></div>

        <div className={classes.topBar}>
          <IconButton className={classes.button} onClick={onClick}>
            <KeyboardReturnIcon />
          </IconButton>

          <div className={classes.titleContainer}>
            <label className={classes.trackName}>{track.fullName}</label>
            <label className={classes.artistName}>{'by ' + artistName}</label>
          </div>
        </div>

        <div className={classes.smalldivider}></div>

        <label className={classes.lyrics}>
          {displayLyrics(track, classes.lyrics, classes.empty)}
        </label>
      </Box>
    );
  } else return '';
};

LyricsDisplay.defaultProps = {
  onClick: null,
};

export default LyricsDisplay;
