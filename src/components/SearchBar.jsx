import React, { useRef } from 'react';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import QyeriesList from './QueriesList';
import { makeStyles } from '@material-ui/core/styles';
import { params } from '../config.json';

const _queryHistory = [];

function addQuery(query) {
  const index = _queryHistory.indexOf(query);
  if (index >= 0) _queryHistory.splice(index, 1);

  _queryHistory.unshift(query);

  if (_queryHistory.length > params.searchHistoryLength) _queryHistory.pop();
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
  },
  inputContainer: {
    color: '#48B37E',
    border: '2px solid #48B37E',
    borderRadius: 20,
    maxWidth: 450,
  },
  input: {
    color: '#48B37E',
    marginLeft: 10,
  },
  button: {
    color: '#48B37E',
  },
  message: {
    textAlign: 'center',
    maxWidth: 400,
    marginTop: '3%',
    fontWeight: 'bold',
    color: '#db7867',
  },
}));

const SearchButton = ({ className, onClick }) => {
  return (
    <IconButton className={className} aria-label="search" onClick={onClick}>
      <SearchIcon />
    </IconButton>
  );
};

const SearchBar = ({ onSearch, placeHolder, message }) => {
  const classes = useStyles();
  const inputRef = useRef(null);
  const [query, setQuery] = React.useState('');
  const [showHistory, setShowHistory] = React.useState(false);

  const submit = (query) => {
    if (onSearch && query !== '') {
      setShowHistory(false);
      onSearch(query);
      addQuery(query);
      setQuery('');
    }
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    inputRef.current.blur();
    submit(query);
  };
  const handleChange = (event) => {
    setQuery(event.target.value);
  };
  const handleQuerySelected = (selected) => {
    submit(selected);
  };

  return (
    <form className={classes.root} onSubmit={handleFormSubmit}>
      <div className={classes.inputContainer}>
        <InputBase
          placeholder={placeHolder}
          fullWidth={true}
          value={query}
          onChange={handleChange}
          onFocus={() => setShowHistory(true)}
          onBlur={() => setShowHistory(false)}
          inputProps={{ className: classes.input, ref: inputRef }}
          endAdornment={
            <SearchButton
              className={classes.button}
              onClick={() => submit(query)}
            />
          }
        />
        <QyeriesList
          className={classes.queriesList}
          expanded={showHistory}
          queries={_queryHistory}
          onSelected={(query) => handleQuerySelected(query)}
        />
      </div>
      <label className={classes.message}>{message}</label>
    </form>
  );
};

SearchBar.defaultProps = {
  placeHolder: 'Search',
  message: 'default message for testing',
};

export default SearchBar;
