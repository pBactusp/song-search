import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'inherit',
  },
}));

const Icon = ({ expand }) => {
  return expand ? <ExpandMoreIcon /> : <ExpandLessIcon />;
};

const CollapseButton = ({ onClick, expand }) => {
  const classes = useStyles();

  return (
    <IconButton
      className={classes.root}
      aria-label="collapse"
      onClick={onClick}
    >
      <Icon expand={expand} />
    </IconButton>
  );
};

CollapseButton.defaultProps = {
  onClick: null,
};

export default CollapseButton;
