import React, { useState } from 'react';
import { getAlbumTracks } from '../services/spotifyService';
import { requestLyrics } from '../services/lyricsService';
import { Paper } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { Box } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import CollapseButton from './CollapseButton';
import Track from './Track';
import LyricsDisplay from './LyricsDisplay';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    margin: 5,
    padding: 2,
    backgroundColor: '#00000044',
    borderColor: 'grey',
    borderWidth: 3,
    color: 'lightGrey',
    transition: [
      ['border-color', '100ms'],
      ['color', '100ms'],
    ],
    '&.selected': {
      borderColor: '#48B37E',
      color: '#48B37E',
    },
  },
  base: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 5,
    margin: 5,
  },
  label: {
    marginLeft: 5,
    fontWeight: 'bold',
  },
  space: {
    flexGrow: 1,
  },
  tracksContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 5,
  },
  divider: {
    borderBottom: '3px solid grey',
  },
  trackDivider: {
    borderBottom: '1px solid grey',
  },
}));

const displayTracks = (
  tracks,
  artistName,
  handleTrackClicked,
  dividerClass
) => {
  if (!tracks) return;

  const lastIndex = tracks.length - 1;
  const components = [];
  for (const track in tracks) {
    components.push(
      <Track
        key={tracks[track].id}
        track={tracks[track]}
        artistName={artistName}
        onClick={handleTrackClicked}
      />
    );

    if (track < lastIndex)
      components.push(
        <div key={'div ' + tracks[track].id} className={dividerClass}></div>
      );
  }

  return components;
};

const Album = ({ album, artistName }) => {
  const classes = useStyles();
  const [selected, setSelected] = useState(false);
  const [currentTrack, setCurrentTrack] = useState(null);

  const handleCollapse = async () => {
    if (!selected && album.tracks == null) {
      album.tracks = await getAlbumTracks(album);
    }

    setSelected(!selected);
  };

  const handleTrackClicked = async (track) => {
    if (!track.lyrics || track.lyrics === '') {
      try {
        track.lyrics = await requestLyrics(artistName, track.name);
      } catch (error) {
        try {
          track.lyrics = await requestLyrics(artistName, track.fullName);
        } catch {
          alert('Something went wrong! Please try again later!');
          return;
        }
      }
    }

    setCurrentTrack(track);
  };

  return (
    <Paper
      className={classes.root + (selected ? ' selected' : '')}
      variant="outlined"
    >
      <div className={classes.base}>
        <Avatar
          className={classes.avatar}
          src={album.images[2].url}
          alt={album.fullName}
        />
        <label className={classes.label}>{album.fullName}</label>
        <span className={classes.space} />
        <CollapseButton expand={selected} onClick={handleCollapse} />
      </div>
      <Collapse in={selected && !currentTrack}>
        <Box className={classes.tracksContainer}>
          <div className={classes.divider}></div>
          {displayTracks(
            album.tracks,
            artistName,
            handleTrackClicked,
            classes.trackDivider
          )}
        </Box>
      </Collapse>
      <Collapse in={selected && currentTrack}>
        <LyricsDisplay
          artistName={artistName}
          track={currentTrack}
          onClick={() => setCurrentTrack(null)}
        />
      </Collapse>
    </Paper>
  );
};

export default Album;
