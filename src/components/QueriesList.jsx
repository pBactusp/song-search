import React from 'react';
import { Box } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import StyledText from './StyledText';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: '0 5px 7px 5px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
  },
  divider: {
    borderBottom: '3px solid grey',
  },
  smalldivider: {
    borderBottom: '2px solid grey',
    margin: '0 5px',
  },
}));

const SearchBar = ({ expanded, queries, onSelected }) => {
  const classes = useStyles();

  const handleSelected = (text) => {
    if (onSelected) onSelected(text);
  };

  const displayQueries = () => {
    const lastIndex = queries.length - 1;
    const components = [];

    for (const query in queries) {
      components.push(
        <StyledText
          key={queries[query]}
          text={queries[query]}
          onClick={handleSelected}
        />
      );
      if (query < lastIndex)
        components.push(
          <div
            className={classes.smalldivider}
            key={'div ' + queries[query]}
          ></div>
        );
    }

    return components;
  };

  if (queries.length > 0) {
    return (
      <Collapse in={expanded}>
        <Box className={classes.root}>
          <div className={classes.divider}></div>
          {displayQueries()}
        </Box>
      </Collapse>
    );
  } else return '';
};

SearchBar.defaultProps = {
  onSelected: null,
  queries: [],
};

export default SearchBar;
