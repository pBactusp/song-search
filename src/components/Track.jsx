import React from 'react';
import StyledText from './StyledText';

const handleClick = (track, onClick) => {
  if (onClick) onClick(track);
};

const Track = ({ track, onClick }) => {
  return (
    <StyledText
      text={`${track.track_number}. ${track.fullName}`}
      onClick={() => handleClick(track, onClick)}
    />
  );
};

Track.defaultProps = {
  onClick: null,
};

export default Track;
